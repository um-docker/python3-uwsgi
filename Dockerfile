# Default configuration for django projects
FROM alpine:latest

MAINTAINER Aleksandr Malov "masashama@gmail.com"

RUN apk add --no-cache python3 python3-dev uwsgi-python3 \
    && ln -s /usr/bin/python3 /usr/bin/python \
    && ln -s /usr/bin/pip3 /usr/bin/pip \
    && mkdir -p /srv 

COPY main.py /srv/main/main.py
COPY uwsgi.ini /etc/uwsgi/uwsgi.ini

EXPOSE 9032
WORKDIR /srv/main

STOPSIGNAL SIGTERM

CMD uwsgi -i /etc/uwsgi/uwsgi.ini